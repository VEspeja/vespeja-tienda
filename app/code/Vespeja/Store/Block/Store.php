<?php

namespace Vespeja\Store\Block;

use Magento\Framework\View\Element\Template;

class Store extends Template
{

    /**
     * @var \Magento\Store\Model\Information
     */
    protected $_storeInfo;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManagerInterface;

    /**
     * @var \Vespeja\Store\Helper\Store
     */
    protected $_storeHelper;

    /**
     * Store constructor.
     * @param \Magento\Store\Model\Information $storeInfo
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
     * @param \Vespeja\Store\Helper\Store $storeHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\Information $storeInfo,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Vespeja\Store\Helper\Store $storeHelper,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_storeInfo = $storeInfo;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_storeHelper = $storeHelper;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPhoneNumber()
    {
        return $this->_storeHelper->getPhoneNumber();
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getName()
    {
        return $this->_storeHelper->getName();
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getLocation()
    {
        return $this->_storeHelper->getLocation();
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCountry()
    {
        return $this->_storeHelper->getCountry();
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getHours()
    {
        return $this->_storeHelper->getHours();
    }

    /**
     * @return mixed
     */
    public function getStoreEmail()
    {
        return $this->_storeHelper->getStoreEmail();
    }
}

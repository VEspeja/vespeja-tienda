<?php

namespace Vespeja\Store\Helper;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\Information;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Store extends Template
{

    /**
     * @var Information
     */
    protected $storeInfo;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Store constructor.
     * @param Information $storeInfo
     * @param StoreManagerInterface $storeManager
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Information $storeInfo,
        StoreManagerInterface $storeManager,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeInfo = $storeInfo;
        $this->storeManager = $storeManager;
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getPhoneNumber()
    {
        $store = $this->storeManager->getStore();
        return $this->storeInfo->getStoreInformationObject($store)->getPhone();
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getName()
    {
        $store = $this->storeManager->getStore();
        return $this->storeInfo->getStoreInformationObject($store)->getName();
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getLocation()
    {
        $store = $this->storeManager->getStore();
        return $this->storeInfo->getStoreInformationObject($store)->getData('street_line1') . ' ' .
            $this->storeInfo->getStoreInformationObject($store)->getData('street_line2') . ',' .
            $this->storeInfo->getStoreInformationObject($store)->getPostcode() . ' - ' .
            $this->storeInfo->getStoreInformationObject($store)->getCity() . ' ' .
            $this->storeInfo->getStoreInformationObject($store)->getRegion();
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getCountry()
    {
        $store = $this->storeManager->getStore();
        return '(' . $this->storeInfo->getStoreInformationObject($store)->getCountry() . ')';
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getHours()
    {
        $store = $this->storeManager->getStore();
        return $this->storeInfo->getStoreInformationObject($store)->getHours();
    }

    /**
     * @return mixed
     */
    public function getStoreEmail()
    {
        return $this->_scopeConfig->getValue(
            'trans_email/ident_sales/email',
            ScopeInterface::SCOPE_STORE
        );
    }
}

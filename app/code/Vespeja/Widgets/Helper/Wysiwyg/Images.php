<?php

namespace Vespeja\Widgets\Helper\Wysiwyg;

class Images extends \Magento\Cms\Helper\Wysiwyg\Images
{

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Backend\Helper\Data $backendData, \Magento\Framework\Filesystem $filesystem, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Escaper $escaper)
    {
        parent::__construct($context, $backendData, $filesystem, $storeManager, $escaper);
    }

    public function getImageHtmlDeclaration($filename, $renderAsTag = false)
    {
        $fileUrl = $this->getCurrentUrl() . $filename;
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $mediaPath = str_replace($mediaUrl, '', $fileUrl);
        $directive = sprintf('{{media url="%s"}}', $mediaPath);
        if ($renderAsTag) {
            $html = sprintf('<img src="%s" alt="" />', $directive);
        } else {
            if ($this->isUsingStaticUrlsAllowed()) {
                //do that the url dont have media directory
                $html = $mediaPath;
            } else {
                $directive = $this->urlEncoder->encode($directive);
                $html = $this->_backendData->getUrl(
                    'cms/wysiwyg/directive',
                    [
                        '___directive' => $directive,
                        '_escape_params' => false,
                    ]
                );
            }
        }
        return $html;
    }
}

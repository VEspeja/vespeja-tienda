<?php

namespace Vespeja\Widgets\Helper;

use Vespeja\Widgets\Plugin\BugFix\WidgetFixDeclarations;
use Magento\Framework\App\Helper\AbstractHelper;


class CmsFix extends AbstractHelper
{

    /**
     * Fix given CMS content
     * @param string $cmsContent CMS Page Content to be fixed
     * @return string Fixed CMS Page Content
     */
    public function fixCmsContent($cmsContent)
    {
        return str_replace(
            array_values(WidgetFixDeclarations::WIDGETFIX_DICT),
            $this->encodeHtmlArray(array_keys(WidgetFixDeclarations::WIDGETFIX_DICT)),
            $cmsContent
        );
    }

    /**
     * Encode given array content by HTML escaping
     * @param array $array Array to be encode
     * @return array Encoded array
     */
    private function encodeHtmlArray($array)
    {
        $encodedArray = [];

        foreach ($array as $item) {
            $encodedArray[] = htmlentities($item);
        }

        return $encodedArray;
    }
}

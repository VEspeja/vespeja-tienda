<?php


namespace Vespeja\Widgets\Block\Home;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Widget\Block\BlockInterface;
use Vespeja\Widgets\Helper\Data;

class MainBanner extends Template implements BlockInterface
{
    protected $_template = 'home/main_banner.phtml';

    /**
     * @var Data
     */
    private $helperBuilderWidget;

    /**
     * MainBanner constructor.
     * @param Template\Context $context
     * @param Data $helperBuilderWidget
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Data $helperBuilderWidget,
        array $data = []
    )
    {
        $this->helperBuilderWidget = $helperBuilderWidget;
        parent::__construct($context, $data);
    }

    /**
     * @param $imageName
     * @return string
     * @throws NoSuchEntityException
     */
    public function getImageUrl($imageName)
    {
        return $this->helperBuilderWidget->getImageUrl($imageName);
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $items = [];
        $data = $this->getData();
        for ($i = 1; $i <= 3; $i++) {
            if (isset($data['btn_label_' . $i]) && isset($data['btn_link_' . $i])) {
                $items[$i] = [
                    'button_label' => $data['btn_label_' . $i],
                    'button_link' => $data['btn_link_' . $i]
                ];
            }
        }
        return $items;
    }

}

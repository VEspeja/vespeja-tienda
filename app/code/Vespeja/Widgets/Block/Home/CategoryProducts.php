<?php

namespace Vespeja\Widgets\Block\Home;

use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;
use Vespeja\Widgets\Block\Template\AbstractProduct;

class CategoryProducts extends AbstractProduct implements BlockInterface
{
    protected $_template = 'home/category_products.phtml';

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * @var \Magento\Catalog\Helper\ImageFactory
     */
    protected $imageHelperFactory;
    /**
     * @var \Magento\Catalog\Api\Data\CategoryInterface
     */
    private $currentCategory;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteria;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;
    /**
     * @var \Magento\Catalog\Helper\Data
     */
    protected $taxHelper;

    /**
     * CategoryProducts constructor.
     * @param Context $context
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteria
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
     * @param \Magento\Catalog\Helper\Data $taxHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteria,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        \Magento\Catalog\Helper\Data $taxHelper,
        array $data = []
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->categoryRepository = $categoryRepository;
        $this->searchCriteria = $searchCriteria;
        $this->productRepository = $productRepository;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->taxHelper = $taxHelper;

        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCategoryProducts()
    {
        $categoryId = explode('/', $this->getData('category_id'))[1];

        $products = [];

        if (isset($categoryId)) {
            try {
                $this->currentCategory = $this->categoryRepository->get($categoryId);

                $searchCriteria = $this->searchCriteria
                    ->addFilter('status', ProductStatus::STATUS_ENABLED)
                    ->addFilter('type_id', 'virtual', 'neq')
                    ->addFilter('category_id', $categoryId)->create()
                    ->setPageSize(6);

                $products = $this->productRepository->getList($searchCriteria)->getItems();
            } catch (\Exception $e) {
            }
        }

        return $products;
    }

    /**
     * @return array|mixed|string|null
     */
    public function getCategoryTitle()
    {
        return (!empty($this->getData('title'))) ? $this->getData('title') : '';
    }

    /**
     * @param $product
     * @return string
     */
    public function getImageUrl($product)
    {
        return $this->imageHelperFactory->create()->init($product, 'product_base_image')->getUrl();
    }

    /**
     * @return string
     */
    public function getCategoryUrl()
    {
        return $this->currentCategory->getUrl() ? $this->currentCategory->getUrl() : '';
    }
}

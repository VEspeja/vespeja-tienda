<?php

namespace Vespeja\Widgets\Block\Home;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Vespeja\Widgets\Helper\Data;

class Principal extends Template implements BlockInterface
{
    protected $_template = 'home/principal.phtml';

    /**
     * @var Data
     */
    private $helperBuilderWidget;

    /**
     * Principal constructor.
     * @param Template\Context $context
     * @param Data $helperBuilderWidget
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Data $helperBuilderWidget,
        array $data = []
    )
    {
        $this->helperBuilderWidget = $helperBuilderWidget;
        parent::__construct($context, $data);
    }

    /**
     * @param $imageName
     * @return string
     * @throws NoSuchEntityException
     */
    public function getImageUrl($imageName)
    {
        return $this->helperBuilderWidget->getImageUrl($imageName);
    }

}

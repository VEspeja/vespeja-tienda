<?php

namespace Vespeja\Widgets\Block\Banner;

class ImagePicker extends \Magento\Backend\Block\Template
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * ImagePicker constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        $data = []
    )
    {
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }


    /**
     * Prepare chooser element HTML
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return \Magento\Framework\Data\Form\Element\AbstractElement
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepareElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $config = $this->_getData('config');
        $sourceUrl = $this->getUrl('cms/wysiwyg_images/index',
            [
                'target_element_id' => $element->getId(),
                'type' => 'file'
            ]);

        /** @var \Magento\Backend\Block\Widget\Button $chooser */
        $chooser = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
            ->setType('button')
            ->setClass('btn-chooser')
            ->setLabel($config['button']['open'])
            ->setOnClick('MediabrowserUtility.openDialog(\'' . $sourceUrl . '\')')
            ->setDisabled($element->getReadonly());

        /** @var \Magento\Framework\Data\Form\Element\Text $input */
        $input = $this->elementFactory->create("text", ['data' => $element->getData()]);
        $input->setId($element->getId());
        $input->setForm($element->getForm());
        $input->setClass("widget-option input-text admin__control-text");


        // Create an image preview
        $preview = $this->getLayout()->createBlock('Vespeja\Widgets\Block\Widget\Form\ImagePreview');

        // If a image is set, preview it
        if ($input->getData('value')) {
            $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $preview->setSource($mediaDirectory . $input->getData('value'));
        }

        if ($element->getRequired()) {
            $input->addClass('required-entry');
        }

        // Include content in html output
        $element->setData('after_element_html', $preview->toHtml() . $input->getElementHtml() . $chooser->toHtml() .
            '<style>
                .admin__field-control.control .control-value {
                    display: none !important;
                }
             </style>' . "<script>require(['mage/adminhtml/browser']);</script>");


        return $element;
    }
}

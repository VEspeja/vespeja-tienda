<?php

namespace Vespeja\Widgets\Block\Widget\Form;


class ImagePreview extends \Magento\Backend\Block\Template
{
    protected $_template = 'Vespeja_Widgets::form/imagepreview.phtml';

    /**
     * @var string Image url
     */
    private $source;

    /**
     * Set Image URL
     * @param string $source Image URL
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string Image URL
     */
    public function getSource()
    {
        return $this->source;
    }

}

<?php

namespace Vespeja\Widgets\Plugin\BugFix;

class WidgetFixDeclarations
{
    const WIDGETFIX_DICT = [
        '"' => '%quot;%',
        '{' => '%brac_op%',
        '}' => '%brac_cl%'
    ];


    /**
     * Modify widget params for fixing quote bug
     * @param \Magento\Widget\Model\Widget $widget
     * @param string $type Widget type
     * @param array $params Widget params
     * @param string $asIs Widget alias
     * @return array
     */
    public function beforeGetWidgetDeclaration(\Magento\Widget\Model\Widget $widget, $type, $params, $asIs)
    {
        foreach ($params as $paramKey => $paramVal){
            $params[$paramKey] = str_replace(array_keys(self::WIDGETFIX_DICT), array_values(self::WIDGETFIX_DICT), $params[$paramKey]);
        }

        return [$type, $params, $asIs];
    }

}

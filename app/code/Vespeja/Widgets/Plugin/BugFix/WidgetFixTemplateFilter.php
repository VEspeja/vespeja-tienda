<?php

namespace Vespeja\Widgets\Plugin\BugFix;

use Magento\Widget\Model\Template\Filter;
use Vespeja\Widgets\Helper\CmsFix;

class WidgetFixTemplateFilter
{

    /**
     * @var CmsFix
     */
    private $helperCmsFix;

    /**
     * WidgetTemplateFilter constructor.
     * @param CmsFix $helperCmsFix
     */
    public function __construct(
        CmsFix $helperCmsFix
    ) {
        $this->helperCmsFix = $helperCmsFix;
    }

    /**
     * @param Filter $subject
     * @param $construction
     * @return mixed
     */
    public function beforeGenerateWidget(Filter $subject, $construction)
    {
        if (isset($construction[2])) {
            $construction[2] = $this->helperCmsFix->fixCmsContent($construction[2]);
        }

        return [$construction];
    }
}

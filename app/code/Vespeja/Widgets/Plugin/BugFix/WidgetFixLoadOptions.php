<?php

namespace Vespeja\Widgets\Plugin\BugFix;

use \Vespeja\Widgets\Plugin\BugFix\WidgetFixDeclarations;

class WidgetFixLoadOptions
{
    /**
     * Modify data that is going to populate widget values
     * @param \Magento\Widget\Block\Adminhtml\Widget\Options $widgetOptions Admin widget block
     * @param array $data Data to be set
     * @param mixed $value Value of data
     * @return array Modified data
     */
    public function beforeSetData(\Magento\Widget\Block\Adminhtml\Widget\Options $widgetOptions, $data, $value)
    {
        if ($data == 'widget_values' && is_array($value)){
            foreach ($value as $valueKey=>$valueVal){
                $value[$valueKey] = str_replace(
                    array_values(WidgetFixDeclarations::WIDGETFIX_DICT),
                    array_keys(WidgetFixDeclarations::WIDGETFIX_DICT),
                    $value[$valueKey]
                );
            }
        }

        return [$data, $value];
    }

}

<?php

namespace Vespeja\Widgets\Plugin;

class StaticImageUrlPlugin
{
    /**
     * Check if the store is configured to use static URLs for media
     * @param \Magento\Catalog\Helper\Data $data
     * @param callable $call
     * @return bool
     */
    public function aroundIsUsingStaticUrlsAllowed(\Magento\Catalog\Helper\Data $data, callable $call)
    {
        return true;
    }
}

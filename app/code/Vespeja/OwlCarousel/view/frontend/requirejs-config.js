var config = {
    paths: {
        "owlcarousel" : "Vespeja_OwlCarousel/js/owl.carousel.min"
    },
    shim: {
        "owlcarousel": {
            deps: ["jquery"]
        }
    }
};

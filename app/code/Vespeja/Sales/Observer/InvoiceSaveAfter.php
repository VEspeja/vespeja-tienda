<?php

namespace Vespeja\Sales\Observer;

use Laminas\Mime\Mime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Sales\Model\Order\Pdf\Invoice;
use Magento\Sales\Model\ResourceModel\Order\Invoice as InvoiceResource;

class InvoiceSaveAfter implements ObserverInterface
{
    const INVOICE_EMAIL_TEMPLATE = 'invoice_new_custom';

    /**
     * @var Invoice
     */
    protected $pdfInvoice;
    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var InvoiceResource
     */
    protected $invoiceResource;

    protected $customerRepository;

    /**
     * InvoiceSaveAfter constructor.
     * @param InvoiceResource $invoiceResource
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param Invoice $pdfInvoice
     */
    public function __construct(
        InvoiceResource $invoiceResource,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        Invoice $pdfInvoice,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    ) {
        $this->invoiceResource = $invoiceResource;
        $this->scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
        $this->pdfInvoice = $pdfInvoice;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $invoice = $observer->getInvoice();
        if (!$invoice->getEmailSent() && !$invoice->getSendEmail()) {
            $invoice->setSendEmail(true);
            try {
                $this->invoiceResource->saveAttribute($invoice, 'send_email');
            } catch (\Exception $e) {
            }
        }
        $this->sendInvoiceMail($invoice);
    }

    /**
     * @param $invoice
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Zend_Pdf_Exception
     */
    private function sendInvoiceMail($invoice)
    {
        if (isset($invoice) && $customerId = $invoice->getCustomerId()) {
            $customer = $this->customerRepository->getById($customerId);
            $pdf = $this->pdfInvoice->getPdf([$invoice]);
            $pdfData = $pdf->render();
            $generalEmail = $this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $senderInfo = [
                'email' => $generalEmail,
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
            ];

            $order = $invoice->getOrder();
            $vars = [
                'order' => $order,
                'invoice' => $invoice,
                'store' => $order->getStore(),
                'order_data' => [
                    'customer_name' => $order->getCustomerName()
                ]
            ];

            $transport = $this->transportBuilder
                ->setTemplateIdentifier(self::INVOICE_EMAIL_TEMPLATE)
                ->setTemplateOptions([
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ])
                ->setTemplateVars($vars)
                ->setFromByScope($senderInfo)
                ->addTo($customer->getEmail())
                ->getTransport();

            $message = $transport->getMessage();
            $message->setBodyAttachment(
                $pdfData,
                $invoice->getIncrementId() . '.pdf',
                'application/pdf',
                Mime::ENCODING_BASE64
            );

            // Send mail message
            $transport->sendMessage();
        }
    }
}

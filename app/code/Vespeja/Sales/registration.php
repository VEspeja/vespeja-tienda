<?php
use \Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(ComponentRegistrar::MODULE,
    'Vespeja_Sales',
    __DIR__
);

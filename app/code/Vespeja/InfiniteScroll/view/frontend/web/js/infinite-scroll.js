define([
    'jquery',
    'loader'
], function ($, loader) {
    'use strict';

    /**
     * Infinite Scroll Widget Component
     */
    $.widget('hib.infiniteScroll', {
        options: {
            pagerElement: '.pages',
            pagerNextElement: '.pages .next',
            containerElement: '.products-grid',
            containerItemsElement: '.products-grid .product-items',
            threshold: 100,
            distance: null,
            isIE: /msie/gi.test(navigator.userAgent),
            prevScrollPos: null,
            callbackContainer: null,
            updateContainer: null,
            dataType: 'html',
            scrollReference: window,
            itemsWrapper: null
        },

        /**
         * Ajax Visor creation
         * @protected
         */
        _create: function () {
            this.init();
        },

        /**
         * Init function
         */
        init: function () {
            this.triggerInfiniteScrollEvent();

            if (this.isPagerAvailable()) {
                if ($(this.options.containerElement).length) {
                    this._bind();
                } else {
                    console.error('Container \'' + this.options.containerElement + '\' not found!');
                }
            }
        },

        /**
         * Bind events to handle function
         * @private
         */
        _bind: function () {
            this.options.distance = $(this.options.containerElement).get(0).getBoundingClientRect().height;
            this.options.prevScrollPos = this.getScrollPos();

            const scroller = {
                options: {
                    distance: this.options.distance,
                    threshold: this.options.threshold,
                    callback: $.proxy(this.callbackFunction, this)
                },
                updateInitiated: false
            };

            $(this.options.scrollReference).on(
                'scroll',
                $.proxy(this.handleScroll, this, scroller)
            );

            var mobileScroll = this.options.scrollReference != window ? this.options.scrollReference:document;
            $(mobileScroll).on(
                'touchmove',
                $.proxy(this.handleScroll, this, scroller)
            );
        },

        /**
         * Handle scroll event and call the callback function if it satisfies the threshold required
         * @param scroller
         * @param event
         */
        handleScroll: function (scroller, event) {
            if (scroller.updateInitiated) {
                return;
            }

            const scrollPos = this.getScrollPos();

            if (scrollPos === this.options.prevScrollPos) {
                return;
            }

            // Find the pageHeight and clientHeight(the no. of pixels to scroll to make the scrollbar reach max pos)
            const pageHeight = document.documentElement.scrollHeight;
            const clientHeight = this.options.scrollReference != window ?
                0:
                document.documentElement.clientHeight;

            // Check if scroll bar position is just 50px above the max, if yes, initiate an update
            if (scrollPos + scroller.options.threshold + clientHeight > scroller.options.distance) {
                scroller.updateInitiated = true;
                scroller.options.callback(function () {
                    scroller.updateInitiated = false;
                });
            }

            this.options.prevScrollPos = scrollPos;
        },

        /**
         * Callback function where we caught the next page and load it by an ajax request
         */
        callbackFunction: function () {
            if (this.isPagerAvailable()) {
                const nextPageUrl = $(this.options.pagerNextElement).get(0).getAttribute('href');
                this.sendLoadMoreProductsRequest(nextPageUrl);
            }
        },

        /**
         * Load more product with an ajax request
         * @param url
         */
        sendLoadMoreProductsRequest: function (url) {
            $.ajax({
                method: "GET",
                url: (url.indexOf('async') > -1) ? url : (url + ((url.indexOf('?') > -1)? '&' : '?') + 'async=1'),
                dataType: this.options.dataType,
                cache: true,
                showLoader: true,
                loaderContext: $(this.options.containerItemsElement).get(0),
                success: $.proxy(this.ajaxSuccess, this),
            });
        },

        /**
         * Ajax Success function bind
         * @param data
         */
        ajaxSuccess: function (data) {
            this.triggerInfiniteScrollEvent();

            const div = document.createElement('div');
            div.innerHTML = data;

            if($(this.options.pagerElement).length && $(div).find(this.options.pagerElement).length) {
                $(this.options.pagerElement).html($(div).find(this.options.pagerElement).get(0).innerHTML);
            }
            if($(this.options.containerItemsElement).length && $(div).find(this.options.containerItemsElement).length) {
                $(this.options.containerItemsElement).append($(div).find(this.options.containerItemsElement).get(0).innerHTML);
            }

            if(this.options.callbackContainer) {
                this.options.callbackContainer(this.options.containerItemsElement);
            }

            this.init();
        },

        /**
         * Check if page is available
         * @returns {number | jQuery}
         */
        isPagerAvailable: function () {
            return ($(this.options.pagerNextElement).length);
        },

        /**
         * Get actual scroll position
         * @returns {number}
         */
        getScrollPos: function () {
            // Handle scroll position in case of IE differently
            if (this.isIE) {
                if(this.options.scrollReference == window) {
                    return document.documentElement.scrollTop;
                }
                else{
                    return $(this.options.scrollReference).scrollTop();
                }
            } else {
                if(this.options.scrollReference == window){
                    return this.options.scrollReference.pageYOffset;
                }
                else{
                    return $(this.options.scrollReference).scrollTop();
                }
            }
        },

        /**
         * Update container components
         */
        triggerInfiniteScrollEvent: function () {
            if(this.options.updateContainer) {
                $(this.options.updateContainer).trigger('contentUpdated');
            }
        }
    });

    return $.hib.infiniteScroll;
});

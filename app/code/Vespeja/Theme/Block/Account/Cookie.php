<?php


namespace Vespeja\Theme\Block\Account;


use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\View\Element\Template;

class Cookie extends Template
{
    protected $_storeManager;
    protected $_urlInterface;

    /**
     * Cookie constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $urlInterface,
        Template\Context $context,
        array $data = [])
    {
        $this->_storeManager = $storeManager;
        $this->_urlInterface = $urlInterface;
        parent::__construct($context, $data);
    }

    public function getCurrentUrl(){
        return $this->_urlInterface->getCurrentUrl();
    }

    public function isCustomerPage($url){
        return (strpos($url, 'customer'));
    }

    public function isCheckoutPage($url){
        return (strpos($url, 'checkout'));
    }
}

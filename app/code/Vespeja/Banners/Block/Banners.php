<?php


namespace Vespeja\Banners\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Banners extends Template implements BlockInterface
{
    protected $_template = 'banners.phtml';


    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var \Vespeja\Banners\Model\ResourceModel\Banners\CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var \Vespeja\Banners\Helper\Data
     */
    private $helper;

    /**
     * Banners constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Vespeja\Banners\Model\ResourceModel\Banners\CollectionFactory $collectionFactory
     * @param \Vespeja\Banners\Helper\Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Vespeja\Banners\Model\ResourceModel\Banners\CollectionFactory $collectionFactory,
        \Vespeja\Banners\Helper\Data $dataHelper
    )
    {

        $this->scopeConfig = $context->getScopeConfig();
        $this->collectionFactory = $collectionFactory;
        $this->helper = $dataHelper;

        parent::__construct($context);
    }

    /**
     * @return \Vespeja\Banners\Model\ResourceModel\AbstractCollection
     */
    public function getFrontBanners()
    {
        $collection = $this->collectionFactory->create()->addFieldToFilter('status', 1)->addFieldToFilter('store_views', array('in' => array(0, $this->_storeManager->getStore()->getId())));

        if ($ids_list = $this->getBannerBlockArguments()) {
            $collection->addFilter('banners_id', ['in' => $ids_list], 'public');
        }

        return $collection;
    }

    /**
     * @return array|false|string[]
     */
    public function getBannerBlockArguments()
    {

        $list = $this->getBannerList();

        $listArray = [];

        if ($list != '') {
            $listArray = explode(',', $list);
        }

        return $listArray;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaDirectoryUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * @param $image
     * @return string
     */
    public function getBannerImage($image)
    {
        return $this->getMediaDirectoryUrl() . ltrim($image,'\/');
    }

    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->helper->getEnable();
    }

    /**
     * @return \Vespeja\Banners\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }
}

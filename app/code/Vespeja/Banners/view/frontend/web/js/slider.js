define([
    'jquery',
    'owlcarousel'
], function($) {

    function slider(data) {

        /*** Vars ***/
        this.id = data.id;
        this.items= data.items;
        this.loop= data.loop;
        this.nav= data.nav;
        this.pagination= data.pagination;
        this.autoplay= data.autoplay;
        this.autoplayHoverPause= data.autoplayHoverPause;
        this.margin= data.margin;
        this.autoPlaySpeed= data.autoPlaySpeed;
        this.autoPlayTimeout= data.autoPlayTimeout;
        this.dots= data.dots;
        this.slider = false;

        /*** Init ***/
        this.init = function () {
            this.build();
        };

        /*** Manage slider ***/
        this.manage = function () {
            if (!this.slider) { // If mobile and slider not built yet = build
                this.build();
            } else if (this.slider) { // Not mobile but slider built = destroy
                this.destroy();
            }
        };

        /*** Build slider ***/
        this.build = function () {

            this.slider = $(this.id).addClass('owl-carousel owl-theme'); // Add owl slider class (3*)
            this.slider.owlCarousel({ // Initialize slider
                items: this.items,
                loop: this.loop,
                nav :this.nav,
                pagination: this.pagination,
                autoplay: this.autoplay,
                autoplayHoverPause: this.autoplayHoverPause,
                margin: this.margin,
                autoPlaySpeed: this.autoPlaySpeed,
                autoPlayTimeout: this.autoPlayTimeout,
                dots: this.dots
            });
        };

        /*** Destroy slider ***/
        this.destroy = function () {
            this.slider.trigger('destroy.owl.carousel'); // Trigger destroy event (4*)
            this.slider = false; // Reinit slider variable
            $(this.id).removeClass('owl-carousel owl-theme'); // Remove owl slider class (3*)
        };

        this.waitForFinalEvent = function () {
            var timers = {};
            return function (callback, ms, uniqueId) {
                if (!uniqueId) {
                    uniqueId = "Don't call this twice without a uniqueId";
                }
                if (timers[uniqueId]) {
                    clearTimeout (timers[uniqueId]);
                }
                timers[uniqueId] = setTimeout(callback, ms);
            };
        };
    }

    return slider;

});

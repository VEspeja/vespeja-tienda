<?php

/**
 * @author Vespeja Team
 * @copyright Copyright (c) 2018 Vespeja (http://kashyapsoftware.com/)
 * @package Vespeja_Banners
*/

namespace Vespeja\Banners\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Effect implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => __('Please Select Banners Effct')],
            ['value' => 'fade', 'label' => __('fade')],
            ['value' => 'backSlide', 'label' => __('backSlide')],
            ['value' => 'goDown', 'label' => __('goDown')],
            ['value' => 'fadeUp', 'label' => __('fadeUp')],
            ['value' => 'slideOutDown', 'label' => __('slideOutDown')]
        ];
    }
}

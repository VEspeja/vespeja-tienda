<?php

/**
 * @author Vespeja Team
 * @copyright Copyright (c) 2018 Vespeja (http://kashyapsoftware.com/)
 * @package Vespeja_Banners
*/


namespace Vespeja\Banners\Model;

class Banners extends \Magento\Framework\Model\AbstractModel
{


    protected function _construct()
    {
        $this->_init('Vespeja\Banners\Model\ResourceModel\Banners');
    }


    public function getAvailableStatuses()
    {


        $availableOptions = ['1' => 'Enable',
                          '0' => 'Disable'];

        return $availableOptions;
    }
}

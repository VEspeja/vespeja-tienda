<?php

namespace Vespeja\Smtp\Helper;

use Laminas\Mime\Mime;

class Test extends \Magento\Framework\App\Helper\AbstractHelper
{
    const TEST_EMAIL_TEMPLATE = 'test_email';
    const TEST_EMAIL_TO = 'vespeja_smtp/test/testEmail';
    const TEST_EMAIL_FROM = 'vespeja_smtp/test/fromEmail';


    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;


    /**
     * Test constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    )
    {
        parent::__construct($context);

        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
    }



    /**
     * Get test email address
     * @return string
     */
    public function getTestEmailTo()
    {
        return $this->getValue(self::TEST_EMAIL_TO);
    }


    /**
     * Get from test email address
     * @return string
     */
    public function getTestEmailFrom()
    {
        return $this->getValue(self::TEST_EMAIL_FROM);
    }


    /**
     * Send test email
     * @param mixed $senderInfo
     * @param mixed $receiverInfo
     * @return void
     */
    public function sendMail($senderInfo, $receiverInfo)
    {
        $this->inlineTranslation->suspend();

        // Prepare mail message
        $transport = $this->transportBuilder
            ->setTemplateIdentifier(self::TEST_EMAIL_TEMPLATE)
            ->setTemplateOptions([
                'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
            ])
            ->setTemplateVars([])
            ->setFrom($senderInfo)
            ->addTo($receiverInfo['email'], $receiverInfo['name'])
            ->getTransport();

        /** Testing attachment too */
        $attachment = <<<PDF
%PDF-1.4
1 0 obj
  << /Type /Catalog
      /Outlines 2 0 R
      /Pages 3 0 R
  >>
endobj

2 0 obj
  << /Type /Outlines
      /Count 0
  >>
endobj

3 0 obj
  << /Type /Pages
      /Kids [ 4 0 R ]
      /Count 1
  >>
endobj

4 0 obj
  << /Type /Page
      /Parent 3 0 R
      /MediaBox [ 0 0 612 792 ]
      /Contents 5 0 R
      /Resources << /ProcSet 6 0 R
      /Font << /F1 7 0 R >>
  >>
>>
endobj

5 0 obj
  << /Length 73 >>
stream
  BT
    /F1 52 Tf
    25 650 Td
    ( prueba de PDF ) Tj
  ET
endstream
endobj

6 0 obj
  [ /PDF /Text ]
endobj

7 0 obj
  << /Type /Font
    /Subtype /Type1
    /Name /F1
    /BaseFont /Helvetica
    /Encoding /MacRomanEncoding
  >>
endobj

xref
0 8
0000000000 65535 f
0000000009 00000 n
0000000074 00000 n
0000000120 00000 n
0000000179 00000 n
0000000364 00000 n
0000000466 00000 n
0000000496 00000 n

trailer
  << /Size 8
    /Root 1 0 R
  >>
startxref
625
%%EOF
PDF;


        $currentDate = new \DateTime();
        $timestamp = $currentDate->format('YmdHis');

        $message = $transport->getMessage();
        $message->setBodyAttachment(
            $attachment,
            $timestamp.'.pdf',
            'application/pdf',
            Mime::ENCODING_BASE64
        );

        // Send mail message
        $transport->sendMessage();

        $this->inlineTranslation->resume();
    }


    /**
     * Return config value
     * @param string $path
     * @param string $type
     * @return mixed
     */
    public function getValue($path, $type = \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue($path, $type);
    }
}

<?php

namespace Vespeja\Smtp\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Store\Model\ScopeInterface;



class Data extends AbstractHelper
{
    const ENABLED = 'vespeja_smtp/general/enabled';
    const TEST_MODE_ENABLED = 'vespeja_smtp/general/test_mode';
    const IGNORE_MODE_ENABLED = 'vespeja_smtp/general/ignore_mode';
    const TEST_MODE_EMAILS_TO = 'vespeja_smtp/general/test_emails';
    const HOST = 'vespeja_smtp/content/host';
    const AUTH = 'vespeja_smtp/content/auth';
    const SSL = 'vespeja_smtp/content/ssl';
    const PORT = 'vespeja_smtp/content/port';
    const USERNAME = 'vespeja_smtp/content/username';
    const PASSWORD = 'vespeja_smtp/content/password';
    const ALL_CONTENT = 'vespeja_smtp/content/';

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    /**
     * Data constructor.
     * @param Context $context
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(
        Context $context,
        ProductMetadataInterface $productMetadata,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    ) {
        parent::__construct($context);

        $this->productMetadata = $productMetadata;
        $this->encryptor = $encryptor;
    }

    /**
     * @return mixed
     */
    public function isEnabled()
    {
        return $this->getConfig(self::ENABLED);
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->getConfig(self::HOST);
    }

    /**
     * @return mixed
     */
    public function getAuth()
    {
        return $this->getConfig(self::AUTH);
    }

    /**
     * @return mixed
     */
    public function getSsl()
    {
        return $this->getConfig(self::SSL);
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->getConfig(self::PORT);
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->getConfig(self::USERNAME);
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->encryptor->decrypt($this->getConfig(self::PASSWORD));
    }

    /**
     * @return mixed
     */
    public function isTestModeEnabled()
    {
        return $this->getConfig(self::TEST_MODE_ENABLED);
    }

    /**
     * @return mixed
     */
    public function isIgnoreModeEnabled()
    {
        return $this->getConfig(self::IGNORE_MODE_ENABLED);
    }

    /**
     * @return array
     */
    public function getTestEmailsTo()
    {
        $emails = $this->getConfig(self::TEST_MODE_EMAILS_TO);

        if(!empty($emails)) {
            return explode(',', $emails);
        }

        return [];
    }

    /**
     * @return array|bool
     */
    public function getSmtpConfig()
    {
        $smtpConfig = [
            'host' => $this->getHost(),
            'auth' => $this->getAuth(),
            'ssl' => $this->getSsl(),
            'port' => $this->getPort(),
            'username' => $this->getUsername(),
            'password' => $this->getPassword()
        ];

        return (count(array_filter($smtpConfig)) == count($smtpConfig))
            ? $smtpConfig
            : false;
    }

    /**
     * @param $ver string
     * @param string $operator
     * @return mixed
     */
    public function versionCompare($ver, $operator = '>=')
    {
        $version = $this->productMetadata->getVersion(); //will return the magento version

        return version_compare($version, $ver, $operator);
    }

    /**
     * @param $config_path
     * @return mixed
     */
    private function getConfig($config_path)
    {
        return $this->scopeConfig->getValue($config_path, ScopeInterface::SCOPE_STORE);
    }

}

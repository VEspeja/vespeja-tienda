<?php

namespace Vespeja\Smtp\Controller\Adminhtml\Mail;

class SendTest extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Vespeja\Smtp\Helper\Test
     */
    private $tester;

    /**
     * Send constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Mail\Message $message
     * @param \Vespeja\Smtp\Helper\Test $tester
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Vespeja\Smtp\Helper\Test $tester
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->tester = $tester;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $email = $this->_request->getParam('to', $this->tester->getTestEmailTo());
        $fromEmail = $this->_request->getParam('from', $this->tester->getTestEmailFrom());

        $receiverInfo = [
            'name' => 'Victor Espeja',
            'email' => $email
        ];

        $senderInfo = [
            'name' => 'Victor',
            'email' => $fromEmail,
        ];

        // Send test message
        $this->tester->sendMail($senderInfo, $receiverInfo);

        // Return response with operation result
        return $this->resultJsonFactory->create()->setData([]);
    }
}

<?php
/**
 * @author: daniDLL
 * Date: 10/9/19
 * Time: 10:32
 */

namespace Vespeja\Smtp\Model\Source;

use Laminas\Mail\Message;
use Zend_Mail_Transport_Abstract;

class Mail
{

    /**
     * @var \Vespeja\Smtp\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Vespeja\Smtp\Model\Strategy\SmtpStrategy
     */
    protected $smtpStrategy;

    /**
     * @var Zend_Mail_Transport_Abstract
     */
    protected $_transport;

    /**
     * Mail constructor.
     * @param \Vespeja\Smtp\Helper\Data $helper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Vespeja\Smtp\Model\Strategy\SmtpStrategy $smtpStrategy
     */
    public function __construct(
        \Vespeja\Smtp\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Vespeja\Smtp\Model\Strategy\SmtpStrategy $smtpStrategy
    ) {
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->smtpStrategy = $smtpStrategy;
    }

    /**
     * @return \Laminas\Mail\Transport\TransportInterface
     */
    public function getTransport()
    {
        if(is_null($this->_transport)) {
            if($this->helper->isEnabled() && $this->helper->getSmtpConfig()) {
                $this->_transport = $this->smtpStrategy;
                return $this->_transport;
            }
        }

        return $this->_transport;
    }

    /**
     * @param $message \Laminas\Mail\Message
     * @return \Laminas\Mail\Message
     */
    public function processMessage($message)
    {
        if($this->helper->isEnabled() && $this->helper->isTestModeEnabled() && !empty($testToEmails = $this->helper->getTestEmailsTo())) {
            $message->setTo($testToEmails);
        }

        return $message;
    }
}

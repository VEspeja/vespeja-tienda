<?php

namespace Vespeja\Smtp\Model\Strategy;

use Laminas\Mail;
use Laminas\Mail\Transport\TransportInterface;
use Laminas\Mail\Transport\Smtp;
use Laminas\Mail\Transport\SmtpOptions;
use Zend_Mail_Transport_Smtp;


class SmtpStrategy implements TransportInterface, StrategyInterface
{
    /**
     * @var \Vespeja\Smtp\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Mail\TransportInterface
     */
    protected $transport;

    /**
     * SmtpStrategy constructor.
     * @param \Vespeja\Smtp\Helper\Data $helper
     */
    public function __construct(
        \Vespeja\Smtp\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Send a mail message
     *
     * @param \Laminas\Mail\Message $message
     * @throws \Exception
     */
    public function send(Mail\Message $message)
    {
        $smtpConfig = $this->helper->getSmtpConfig();

        if($this->helper->versionCompare('2.2.8')) {
            if (isset($smtpConfig['auth'])) {
                $smtpConfig['connection_class'] = $smtpConfig['auth'];
                $smtpConfig['connection_config'] = [
                    'username' => $smtpConfig['username'],
                    'password' => $smtpConfig['password']
                ];
                unset($smtpConfig['auth'], $smtpConfig['username'], $smtpConfig['password']);
            }
            if (isset($smtpConfig['ssl'])) {
                $smtpConfig['connection_config']['ssl'] = $smtpConfig['ssl'];
                unset($smtpConfig['ssl']);
            }
            unset($smtpConfig['type']);

            $smtpConfig = new SmtpOptions($smtpConfig);

            $sender = new Smtp($smtpConfig);
            $sender->send($message);
        } else {
            $sender = new Zend_Mail_Transport_Smtp(
                $smtpConfig['host'],
                $smtpConfig
            );
            $sender->send($this->getMessage($this->getTransport()));
        }
    }

    /**
     * @param $transport
     * @return mixed|null
     */
    protected function getMessage($transport)
    {
        if ($this->helper->versionCompare('2.2.0')) {
            return $transport->getMessage();
        }

        try {
            $reflectionClass = new ReflectionClass($transport);
            $message = $reflectionClass->getProperty('_message');
        } catch (\Exception $e) {
            return null;
        }

        $message->setAccessible(true);

        return $message->getValue($transport);
    }

    /**
     * @param $transport \Magento\Framework\Mail\TransportInterface
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

    /**
     * @return \Magento\Framework\Mail\TransportInterface
     */
    public function getTransport()
    {
        return $this->transport;
    }
}

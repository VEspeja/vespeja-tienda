<?php

namespace Vespeja\Smtp\Model\Strategy;

interface StrategyInterface
{
    /**
     * @param $transport \Magento\Framework\Mail\TransportInterface
     */
    public function setTransport($transport);

    /**
     * @return \Magento\Framework\Mail\TransportInterface
     */
    public function getTransport();
}

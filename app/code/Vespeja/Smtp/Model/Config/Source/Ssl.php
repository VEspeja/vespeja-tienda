<?php

namespace Vespeja\Smtp\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Ssl implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'ssl', 'label' => __('SSL')],
            ['value' => 'tls', 'label' => __('TLS')]
        ];
    }
}

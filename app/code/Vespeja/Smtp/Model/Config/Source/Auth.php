<?php

namespace Vespeja\Smtp\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Auth implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'login', 'label' => __('Login')],
            ['value' => 'plain', 'label' => __('Plain')],
            ['value' => 'crammd5', 'label' => __('CRAM-MD5')]
        ];
    }
}

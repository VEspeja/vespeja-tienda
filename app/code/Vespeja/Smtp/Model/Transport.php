<?php

namespace Vespeja\Smtp\Model;

use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Phrase;

class Transport
{
    /**
     * @var \Vespeja\Smtp\Helper\Data
     */
    protected $helper;

    /**
     * @var \Vespeja\Smtp\Model\Source\Mail
     */
    protected $mailSource;

    /**
     * Transport constructor.
     * @param \Vespeja\Smtp\Helper\Data $helper
     * @param Source\Mail $mailSource
     */
    public function __construct(
        \Vespeja\Smtp\Helper\Data $helper,
        \Vespeja\Smtp\Model\Source\Mail $mailSource
    ) {
        $this->helper = $helper;
        $this->mailSource = $mailSource;
    }

    /**
     * @param TransportInterface $subject
     * @param \Closure $proceed
     * @throws MailException
     * @throws
     */
    public function aroundSendMessage(
        TransportInterface $subject,
        \Closure $proceed
    ) {
        $message = $this->getMessage($subject);

        /** @var \Laminas\Mail\Transport\TransportInterface | \Vespeja\Smtp\Model\Strategy\StrategyInterface $transport */
        if ($this->helper->isEnabled() && $message && !is_null($transport = $this->mailSource->getTransport())) {
            if ($this->helper->versionCompare('2.2.8')) {
                $message = \Laminas\Mail\Message::fromString($message->getRawMessage());
            }

            $message->getHeaders()->get('to')->setEncoding('UTF-8');
            $message = $this->mailSource->processMessage($message);

            try {
                if (!$this->helper->isIgnoreModeEnabled()) {
                    $transport->setTransport($subject);
                    $transport->send($message);
                }

            } catch (\Exception $e) {
                throw new MailException(new Phrase($e->getMessage()), $e);
            }
        } else {
            $proceed();
        }
    }

    /**
     * @param $transport
     * @return mixed|null
     */
    protected function getMessage($transport)
    {
        if ($this->helper->versionCompare('2.2.0')) {
            return $transport->getMessage();
        }

        try {
            $reflectionClass = new ReflectionClass($transport);
            $message = $reflectionClass->getProperty('_message');
        } catch (\Exception $e) {
            return null;
        }

        $message->setAccessible(true);

        return $message->getValue($transport);
    }
}

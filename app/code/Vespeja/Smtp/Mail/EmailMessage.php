<?php

namespace Vespeja\Smtp\Mail;

use Magento\Framework\Mail\Address;
use Magento\Framework\Mail\AddressFactory;
use Magento\Framework\Mail\EmailMessageInterface;
use Magento\Framework\Mail\Exception\InvalidArgumentException;
use Magento\Framework\Mail\MimeMessageInterface;
use Magento\Framework\Mail\MimeMessageInterfaceFactory;
use Laminas\Mail\Address as ZendAddress;
use Laminas\Mail\AddressList;
use Laminas\Mail\Message as ZendMessage;
use Laminas\Mime\Message as ZendMimeMessage;
use Laminas\Mime\Mime;


class EmailMessage implements EmailMessageInterface
{
    /**
     * @var \Laminas\Mime\PartFactory
     */
    private $partFactory;

    /**
     * @var ZendMessage
     */
    private $message;

    /**
     * @var MimeMessageInterfaceFactory
     */
    private $mimeMessageFactory;

    /**
     * @var AddressFactory
     */
    private $addressFactory;

    /**
     * EmailMessage constructor.
     * @param MimeMessageInterface $body
     * @param array $to
     * @param MimeMessageInterfaceFactory $mimeMessageFactory
     * @param AddressFactory $addressFactory
     * @param \Laminas\Mime\PartFactory $partFactory
     * @param array|null $from
     * @param array|null $cc
     * @param array|null $bcc
     * @param array|null $replyTo
     * @param Address|null $sender
     * @param string|null $subject
     * @param string|null $encoding
     */
    public function __construct(
        MimeMessageInterface $body,
        array $to,
        MimeMessageInterfaceFactory $mimeMessageFactory,
        AddressFactory $addressFactory,
        \Laminas\Mime\PartFactory $partFactory,
        ?array $from = null,
        ?array $cc = null,
        ?array $bcc = null,
        ?array $replyTo = null,
        ?Address $sender = null,
        ?string $subject = '',
        ?string $encoding = ''
    ) {
        $this->message = new ZendMessage();
        $mimeMessage = new ZendMimeMessage();

        $this->partFactory = $partFactory;
        $this->mimeMessageFactory = $mimeMessageFactory;
        $this->addressFactory = $addressFactory;

        $mimeMessage->setParts($body->getParts());
        $this->message->setBody($mimeMessage);

        if ($encoding) {
            $this->message->setEncoding($encoding);
        }
        if ($subject) {
            $this->message->setSubject($subject);
        }
        if ($sender) {
            $this->message->setSender($sender->getEmail(), $sender->getName());
        }
        if (count($to) < 1) {
            throw new InvalidArgumentException('Email message must have at list one addressee');
        }
        if ($to) {
            $this->message->setTo($this->convertAddressArrayToAddressList($to));
        }
        if ($replyTo) {
            $this->message->setReplyTo($this->convertAddressArrayToAddressList($replyTo));
        }
        if ($from) {
            $this->message->setFrom($this->convertAddressArrayToAddressList($from));
        }
        if ($cc) {
            $this->message->setCc($this->convertAddressArrayToAddressList($cc));
        }
        if ($bcc) {
            $this->message->setBcc($this->convertAddressArrayToAddressList($bcc));
        }
    }

    /**
     * @inheritDoc
     */
    public function getEncoding(): string
    {
        return $this->message->getEncoding();
    }

    /**
     * @inheritDoc
     */
    public function getHeaders(): array
    {
        return $this->message->getHeaders()->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getFrom(): ?array
    {
        return $this->convertAddressListToAddressArray($this->message->getFrom());
    }

    /**
     * @inheritDoc
     */
    public function getTo(): array
    {
        return $this->convertAddressListToAddressArray($this->message->getTo());
    }

    /**
     * @inheritDoc
     */
    public function getCc(): ?array
    {
        return $this->convertAddressListToAddressArray($this->message->getCc());
    }

    /**
     * @inheritDoc
     */
    public function getBcc(): ?array
    {
        return $this->convertAddressListToAddressArray($this->message->getBcc());
    }

    /**
     * @inheritDoc
     */
    public function getReplyTo(): ?array
    {
        return $this->convertAddressListToAddressArray($this->message->getReplyTo());
    }

    /**
     * @inheritDoc
     */
    public function getSender(): ?Address
    {
        /** @var ZendAddress $zendSender */
        if (!$zendSender = $this->message->getSender()) {
            return null;
        }

        return $this->addressFactory->create(
            [
                'email' => $zendSender->getEmail(),
                'name' => $zendSender->getName()
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function getSubject(): ?string
    {
        return $this->message->getSubject();
    }

    /**
     * @inheritDoc
     */
    public function getBody(): MimeMessageInterface
    {
        return $this->mimeMessageFactory->create(
            ['parts' => $this->message->getBody()->getParts()]
        );
    }

    /**
     * @inheritDoc
     */
    public function getBodyText(): string
    {
        return $this->message->getBodyText();
    }

    /**
     * @inheritdoc
     */
    public function getRawMessage(): string
    {
        return $this->toString();
    }

    /**
     * @inheritDoc
     */
    public function toString(): string
    {
        return $this->message->toString();
    }

    /**
     * Converts AddressList to array
     *
     * @param AddressList $addressList
     * @return Address[]
     */
    private function convertAddressListToAddressArray(AddressList $addressList): array
    {
        $arrayList = [];
        foreach ($addressList as $address) {
            $arrayList[] =
                $this->addressFactory->create(
                    [
                        'email' => $address->getEmail(),
                        'name' => $address->getName()
                    ]
                );
        }

        return $arrayList;
    }

    /**
     * Converts MailAddress array to AddressList
     *
     * @param Address[] $arrayList
     * @return AddressList
     */
    private function convertAddressArrayToAddressList(array $arrayList): AddressList
    {
        $zendAddressList = new AddressList();
        foreach ($arrayList as $address) {
            $zendAddressList->add($address->getEmail(), $address->getName());
        }

        return $zendAddressList;
    }

    /**
     * Add the attachment mime part to the message.
     * @param string $content
     * @param string $fileName
     * @param string $fileType
     * @param string $encoding
     * @return $this
     */
    public function setBodyAttachment($content, $fileName, $fileType, $encoding = '8bit')
    {
        // Get message body
        $mailBody = $this->message->getBody();

        // Create new part with attached file
        $attachmentPart = $this->partFactory->create()
            ->setContent($content)
            ->setType($fileType)
            ->setFileName($fileName)
            ->setDisposition(Mime::DISPOSITION_ATTACHMENT)
            ->setEncoding($encoding);

        // Add attached file to mail body
        $mailBody->addPart($attachmentPart);

        // Update body message
        $this->message->setBody($mailBody);

        return $this;
    }

    /**
     * Return the currently set message body
     * @return MimeMessageInterface
     */
    public function getMessageBody(): MimeMessageInterface
    {
        return $this->message->getBody();
    }

    /**
     * Set mail message body in HTML format.
     * @param string $html
     * @return $this
     * @since 101.0.8
     */
    public function setBodyHtml($html)
    {
        $this->message->setBody($html);
        return $this;
    }

    /**
     * Set mail message body in text format.
     * @param string $text
     * @return $this
     * @since 101.0.8
     */
    public function setBodyText($text)
    {
        $this->message->setBody($text);
        return $this;
    }

    /**
     * Set message subject
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->message->setSubject($subject);
        return $this;
    }

    /**
     * Set message body
     * @param mixed $body
     * @return $this
     * @deprecated 102.0.0
     * @see \Magento\Framework\Mail\MailMessageInterface::setBodyHtml
     * @see \Magento\Framework\Mail\MailMessageInterface::setBodyText()
     */
    public function setBody($body)
    {
        $this->message->setBody($body);
        return $this;
    }

    /**
     * Set from address
     * @param string|array $fromAddress
     * @return $this
     */
    public function setFrom($fromAddress)
    {
        $this->message->setFrom($fromAddress);
        return $this;
    }

    /**
     * Add to address
     * @param string|array $toAddress
     * @return $this
     */
    public function addTo($toAddress)
    {
        $this->message->addTo($toAddress);
        return $this;
    }

    /**
     * Add cc address
     * @param string|array $ccAddress
     * @return $this
     */
    public function addCc($ccAddress)
    {
        $this->message->addCc($ccAddress);
        return $this;
    }

    /**
     * Add bcc address
     * @param string|array $bccAddress
     * @return $this
     */
    public function addBcc($bccAddress)
    {
        $this->message->addBcc($bccAddress);
        return $this;
    }

    /**
     * Set reply-to address
     * @param string|array $replyToAddress
     * @return $this
     */
    public function setReplyTo($replyToAddress)
    {
        $this->message->setReplyTo($replyToAddress);
        return $this;
    }

    /**
     * Set message type
     * @param string $type
     * @return $this
     * @deprecated 102.0.0
     * @see \Magento\Framework\Mail\MailMessageInterface::setBodyHtml
     * @see \Magento\Framework\Mail\MailMessageInterface::getBodyHtml
     * @see \Magento\Framework\Mail\MailMessageInterface::setBodyText()
     * @see \Magento\Framework\Mail\MailMessageInterface::getBodyText()
     */
    public function setMessageType($type)
    {
        return $this;
    }
}
